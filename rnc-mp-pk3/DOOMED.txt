A list of all DoomED numbers assigned to actors in this map pack
to identify any IDs that make the pack incompatible with others.

22001 = RNCFLA Plant
22002 = RNCFLA Plant 2

22003 = RNCCLA Post
22004 = RNCCLA Platform

22005 = RNCBUB Wind Bubble Spawner
22018 = RNCBUB Spike

22006 = RNCMET Acid Dripper

22007 = RNCQUI Shiny Crystal
22008 = RNCQUI Shiny Floor Cryatal
22009 = RNCQUI Shiny Ceiling Crystal
22010 = RNCQUI Big Shiny Crystal
22011 = RNCQUI Shiny Beam Shooter

22012 = RNCWOO Tree 1
22013 = RNCWOO Tree 2
22014 = RNCWOO Wily Snowman

22015 = RNCDW1 Spikes
22016 = RNCDW1 Chains

22017 = RNCDW3 Floor Spikes
22019 = RNCDW3 Ceiling Spikes