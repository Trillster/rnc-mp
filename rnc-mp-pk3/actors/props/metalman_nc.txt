Actor AcidDripperNC : AcidDripper 22006
{
//$Category RNC-Props
//$Title MetalNC Acid Dripper
States
{
Spawn:
NCP2 F 0
NCP2 FG 4
NCP2 G 0 A_LookEx(LOF_NOSOUNDCHECK,0,128,0,0,1)
loop
Drip:
NCP2 HI 4
NCP2 J 4 A_CustomMissile("AcidDripNC",0,0,0,2,0)
NCP2 J 0 A_ClearTarget
NCP2 KJFGFGFG 4 
Goto Spawn
}
}

actor AcidDripNC : AcidDrip
{
States
{
Spawn:
NCP2 L 1 A_CheckFloor("Death")
loop
Death:
NCP2 M 6 A_PlaySoundEx("misc/aciddrip","Voice")
NCP2 N 6
stop
}
}

actor BubbleBatNC
{
PROJECTILE
Damage 0
+DONTBLAST
+RIPPER
Radius 5
Height 5
+BLOODLESSIMPACT
+CLIENTSIDEONLY
Scale 2.5
Speed 8
-SOLID
+NOGRAVITY
States
{
Spawn:
NCP2 OP 4
NCP2 P 0 A_Jump(30,"Up")
NCP2 P 0 A_Jump(30,"Down")
NCP2 Q 4
loop
Up:
NCP2 Q 4 ThrustThingZ(0,2,0,1)
goto Spawn
Down:
NCP2 Q 4 ThrustThingZ(0,-2,0,1)
goto Spawn
}
}