map RNCAIR "Airman No Constancy" 
{
    next = "RNCHEA" 	
    sky1 = "BLACK", 0
    music = "AIRMUSNC" 
    aircontrol = 0.5
}

map RNCHEA "Heatman No Constancy"
{
    next = "RNCFLA" 	
    sky1 = "BLACK", 0
    music = "HEAMUSNC" 
    aircontrol = 0.5
}

map RNCFLA "Flashman No Constancy"
{
    next = "RNCCLA" 	
    sky1 = "BLACK", 0
    music = "FLAMUSNC" 
    aircontrol = 0.5
}

map RNCCLA "Clashman No Constancy"
{
    next = "RNCBUB" 	
    sky1 = "BLACK", 0 
    music = "CLAMUSNC" 
    aircontrol = 0.5
}

map RNCBUB "Bubbleman No Constancy"
{
    next = "RNCMET" 	
    sky1 = "BLACK", 0 
    music = "BUBMUSNC" 
    aircontrol = 0.5
}

map RNCMET "Metalman No Constancy"
{
    next = "RNCQUI" 	
    sky1 = "BLACK", 0 
    music = "METMUSNC" 
    aircontrol = 0.5
}

map RNCQUI "Quickman No Constancy"
{
    next = "RNCWOO" 	
    sky1 = "BLACK", 0 
    music = "QUIMUSNC" 
    aircontrol = 0.5
}

map RNCWOO "Woodman No Constancy"
{
    next = "RNCDW1" 	
    sky1 = "BLACK", 0 
    music = "WOOMUSNC" 
    aircontrol = 0.5
}

map RNCDW1 "Wily 1 No Constancy"
{
    next = "RNCDW2" 	
    sky1 = "BLACK", 0 
    music = "DW1MUSNC" 
    aircontrol = 0.5
}

map RNCDW2 "Wily 2 No Constancy"
{
    next = "RNCDW3" 	
    sky1 = "BLACK", 0 
    music = "DW1MUSNC" 
    aircontrol = 0.5
}

map RNCDW3 "Wily 3 No Constancy"
{
    next = "RNCDW4" 	
    sky1 = "BLACK", 0 
    music = "DW3MUSNC" 
    aircontrol = 0.5
}

map RNCDW4 "Wily 4 No Constancy"
{
    next = "RNCAIR" 	
    sky1 = "BLACK", 0 
    music = "DW3MUSNC" 
    aircontrol = 0.5
}